#!/bin/bash

echo "### Activation du service traefik pour ingres port 8080 ###"
microk8s enable traefik
echo "export env variables"
export NB_REPLICA_FRONT=4
export NB_REPLICA_BACK=2
export TAG_IMAGE_FRONT=barifaga/meteofront:114
export TAG_IMAGE_BACK=barifaga/meteoback:114
echo "Launch sonicastScalSetProd.yml to create and list prod deploy ressources"
envsubst < sonicastScalSetProd.yml | /var/lib/snapd/snap/bin/microk8s kubectl apply -f -
/var/lib/snapd/snap/bin/microk8s kubectl apply -f service.yml
/var/lib/snapd/snap/bin/microk8s kubectl apply -f ingress.yml
/var/lib/snapd/snap/bin/microk8s kubectl get deploy,po,svc,ingress -o wide